const controller= require('./controller')


//where we define our rest routes
module.exports = (app) => {
    app.get('*', controller.getResource)
}
