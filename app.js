const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const routes = require('./routes')

const app = express()
const port = process.env.PORT || 3000
// setup middleware
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())

routes(app)

app.listen(port, () => console.log(`listening on port ${port}`))

