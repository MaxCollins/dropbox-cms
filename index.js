const express = require('express')
const app = express()
const port = process.env.PORT || 3000
app.listen(port)

// //gets the mongoose db
// var mongoose = require('mongoose');
// mongoose.connect('mongodb://localhost/test');
// var db = mongoose.connection;
// db.on('error', console.error.bind(console, 'connection error:'));
// db.once('open', function() {
// });
var token = process.env.TOKEN || 'K-T9MVav7IAAAAAAAAAA_dT19kAwCIhdF0hDEsd9wm9MG8ewMAUlpBtz0I_abOJ3';

//uses body-parser
const bodyParser = require('body-parser');

//uses js-yaml
yaml = require('js-yaml');
fs   = require('fs');

//uses Gray Matter
const matter = require('gray-matter');
//get dropbox data
require('isomorphic-fetch'); 
var Dropbox = require('dropbox').Dropbox;
var dbx = new Dropbox({ accessToken: 'K-T9MVav7IAAAAAAAAAA_dT19kAwCIhdF0hDEsd9wm9MG8ewMAUlpBtz0I_abOJ3' });

// function getAllDropboxAssets(){
//   return new Promise((resolve, reject)=>{
//     dbx.filesListFolder({path: '', recursive: true})
//       .then(function(response) {
//         resolve(response);
//       })
//       .catch(function(error){
//         reject(error);
//       });
//   });
// }


main();

async function main(){
  
	try {
	const listAllResponse = await dbx.filesListFolder({path: '', recursive: true})
	// console.log(filesAndFolders);
	console.log(listAllResponse);
  const entries = listAllResponse.entries;
  const folders = [];
  for (let entry of listAllResponse.entries) {
    if(entry['.tag'] === "file") {
      const getFileResponse = await dbx.filesDownload({path: entry.path_lower});
      const fileData = getFileResponse.fileBinary;
      const fileExtension = entry.name.substr(entry.name.lastIndexOf('.'));
      if (['.md'].includes(fileExtension)) {
        var content = matter((new Buffer(fileData)));
        var object = Object.assign({}, {...content.data}, {body: content.content}, {id: extensionChopper(entry.name)})
        entry["content"] = object;
      }
      else if (['.yml', '.yaml'].includes(fileExtension)) {
        var content = yaml.safeLoad((new Buffer(fileData)));
        var object = Object.assign({}, {id: extensionChopper(entry.name)})
        for (var prop in content) {
          object[prop] = content[prop];
        };

        //var object = Object.assign({},{content}, {id: extensionChopper(entry.name)})
        //console.log(object);
        entry["content"] = object;
      }
      else {
          entry["content"] = JSON.parse(new Buffer(fileData).toString('utf8'));
      }
    }
    if(entry['.tag'] === "folder") {
      folders.push(entry["path_lower"]);
      
    }
  }

   //sends all files in cms if at base route
   const totalFiles = entries.filter((entry) => {
      if (entry['.tag'] === 'file') {
          return entry;
      }
    })
    app.get('/', (req, res) => res.send(totalFiles))

    //gets all files and folders within folder
    for ( let folderName of folders ) {
        const filteredFiles = entries.filter((entry) => {
            if (entry.path_lower.includes(folderName)  && entry['path_lower'] != folderName) {
                return entry;
            }
        })
        app.get(folderName, (req, res) => res.send(filteredFiles))
    }


    //sends single file information if requested
    for ( let entry of entries) {
        app.get(extensionChopper(entry.path_lower), (req, res) => res.send(entry))
    }		
	} 
	catch (error) {
		console.log(error)	
	}
  // var filesAndFolders
}   

//utility functions
function extensionChopper(name) {
    var name = name.split('.')[0];
    return name;
}