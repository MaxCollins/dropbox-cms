const dropBoxService = require('./dropboxService')


exports.getResource = async (req, res) => {
    //the dropbox api doesn't like  '/root' it'll fail unless 'root' or '//root'
    console.log(req.params['0'])
    let filePath = req.params[0];
    if (filePath[0] === req.params[0]){
        filePath = '';
    }

    try {
        const files = await dropBoxService.getFiles(filePath);
        //console.log(files.length)
        if (files.length) {
            return res.status(200).json(files)
        }
        else {
            return res.status(404).send()
        }
    } 
    catch (error) {
        // discussion point DO NOT DELETE
        try {
            const files = await dropBoxService.getFiles(req.params[0] + '.json');
            return res.status(200).json(files)
        } catch(error) {
            try {
                const files = await dropBoxService.getFiles(req.params[0] + '.yml');
                return res.status(200).json(files)
            } catch (error) {
                try {
                    console.log('get here?')
                    const files = await dropBoxService.getFiles(req.params[0] + '.md'); 
                    return res.status(200).json(files)
                } catch (error) {
                    console.log(error)
                    return res.status(500).send(error)
                }
            }
        if (error.message) {
            res.status(500).send(error.message)
        }
        else {
            return res.status(500).send(req.params[0])
        }
    }
    }
}

exports.fallback = (req, res) => {
    return res.send('hit fallback route')
}
