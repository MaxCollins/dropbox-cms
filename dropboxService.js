yaml = require('js-yaml');
fs   = require('fs');
const matter = require('gray-matter');
var token = process.env.TOKEN || 'K-T9MVav7IAAAAAAAAAA_dT19kAwCIhdF0hDEsd9wm9MG8ewMAUlpBtz0I_abOJ3';

//dropbox
require('isomorphic-fetch'); 
var Dropbox = require('dropbox').Dropbox;
var dbx = new Dropbox({ accessToken: token });


const extractFiles = (entries) => {
    return entries.filter((item) => {
        return item['.tag'] === 'file'
    })
}
async function processFile(fileMeta){
    return new Promise(async (resolve, reject) => {
        try {
            const getFileResponse = await dbx.filesDownload({path: fileMeta.path_lower});
            //console.log(fileMeta.name, fileMeta.path_lower, getFileResponse)
            const fileData = getFileResponse.fileBinary;
            const fileExtension = fileMeta.name.substr(fileMeta.name.lastIndexOf('.'));
            if (fileExtension === '.json') {
                fileData.content = readJsonFile(fileData);
                let processedJsonFile = Object.assign({}, {id: extensionChopper(fileMeta.name)}, {...fileData.content},  {meta:{path: fileMeta.path_lower, }})
                resolve(processedJsonFile);
            }
            else if (fileExtension === '.yml') {
                fileData.content = yaml.safeLoad((new Buffer(fileData)));
                let processedFileData = Object.assign({}, {id: extensionChopper(fileMeta.name)}, {...fileData.content},  {meta:{path: fileMeta.path_lower }})
                resolve(processedFileData)
            }
            else if (fileExtension == '.md') {
                fileData.content = matter((new Buffer(fileData)));
                var processedMarkDownData = Object.assign({},{id: extensionChopper(fileMeta.name)}, {...fileData.content}, {meta:{path: fileMeta.path_lower}} )
                for (var prop in processedMarkDownData.data) {
                    processedMarkDownData[prop] = processedMarkDownData.data[prop];
                };
                delete processedMarkDownData.data;
                delete processedMarkDownData.orig;
                delete processedMarkDownData.isEmpty;
                delete processedMarkDownData.exerpt;
                resolve(processedMarkDownData);
            }
            else {
                var nonHandledFileType = Object.assign({}, {id: extensionChopper(fileMeta.name)}, {meta:{path: fileMeta.path_lower}})
                console.log(getFileResponse)
                resolve(nonHandledFileType);
            }
        }
        catch (error) {
            reject(error);
        }
    })
}
function readJsonFile(fileData) {
    var parsedData = JSON.parse(new Buffer(fileData).toString('utf8'))
    return parsedData;
}

exports.getFiles = (folderPath) => {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await dbx.filesListFolder({path: folderPath, recursive: true})
            const items = response.entries;
            const filesMetaData = extractFiles(items)
            var processedFiles = []
            for (const item of filesMetaData) {
                var processedFile= await processFile(item)
                processedFiles.push(processedFile)
            }
            resolve(processedFiles);
        }
        catch (err){
            try {
                const item = {}
                item['path_lower'] = folderPath;
                item['name'] = folderPath.substr(folderPath.lastIndexOf('/'))
                const getFileResponse = await processFile(item)
                resolve(getFileResponse)
            }
            catch(err) {
                reject(err)
            }
        }
    })
}


//utility functions
function extensionChopper(name) {
    var name = name.split('.')[0];
    return name;
}